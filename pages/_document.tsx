import Document, { Html, Head, Main, NextScript, DocumentContext } from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <meta charSet='utf-8' />
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta name='description' content='Covid-19 Reporter' />
          <meta name='keywords' content='covid-reporter' />
          <link rel='manifest' href='/manifest.json' />
          <link href='/favicon-16x16.png' rel='icon' type='image/png' sizes='16x16' />
          <link href='/favicon-32x32.png' rel='icon' type='image/png' sizes='32x32' />
          <link rel='apple-touch-icon' href='/apple-icon.png' />
          <link rel='mask-icon' href='/safari-pinned-tab.svg' color='#343A40' />
          <meta name='msapplication-TileColor' content='#343A40' />
          <meta name='theme-color' content='#343A40' />
          {/* Meta tag for Facebook */}
          <meta property='og:title' content='COVID-19 REPORTER' />
          <meta property='og:description' content='ติดตามสถานการณ์ COVID-19 ในประเทศไทย' />
          <meta property='og:image' content='https://covid-reporter.now.sh/images/cover.png' />
          <meta property='og:url' content='https://covid-reporter.now.sh' />
          <meta property='og:type' content='website' />
          <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' />
          <link href='https://fonts.googleapis.com/css?family=Mitr&display=swap' rel='stylesheet' />
          <script defer={true} src='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js' />
        </Head>
        <body className='bg-dark'>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;