import { NextApiRequest, NextApiResponse } from 'next';
import { config } from '../../../configs';
import request from 'request-promise';
import { CaseSumReport } from '../../../types';

export default async (_req: NextApiRequest, res: NextApiResponse) => {
    try {
        const response: string = await request(config.dcCaseSumReportEndpoint);
        const result: CaseSumReport = JSON.parse(response);
        res.status(200).json(result);
    } catch (err) {
        const { statusCode, message } = err;
        res.status(500).json({ statusCode, message });
    }
};
