import { NextApiRequest, NextApiResponse } from 'next';
import { config } from '../../../configs';
import request from 'request-promise';
import { RapidAPIResponse, DailyReport } from '../../../types';

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const API_KEY = config.rapiadApiKey;
    const country = req.query.country || 'Thailand';
    try {
        const response: string = await request(`${config.rapidApiEndpoint}?country=${country}`, {
            headers: {
                'x-rapidapi-host': config.rapidApiHost,
                'x-rapidapi-key': API_KEY
            }
        });
        const responseDC: string = await request(config.dcDailyReportEndpoint);
        const result: RapidAPIResponse = JSON.parse(response);
        const resultDC: DailyReport = JSON.parse(responseDC);
        result.response[0].cases = {
            ...result.response[0].cases,
            total: resultDC.Confirmed || 0,
            new: resultDC.NewConfirmed || 0,
            newRecovered: resultDC.NewRecovered || 0,
            recovered: resultDC.Recovered || 0,
        };
        result.response[0].deaths = {
            ...result.response[0].deaths,
            total: resultDC.Deaths || 0,
            new: resultDC.NewDeaths || 0,
        };
        res.status(200).json(result);
    } catch (err) {
        const { statusCode, message } = err;
        res.status(500).json({ statusCode, message });
    }
};
