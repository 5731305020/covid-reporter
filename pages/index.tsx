import React, { Component } from 'react';
import Head from 'next/head';
import { NavBar } from '../components/NavBar/NavBar';
import { RapidAPIResponse } from '../types';
import StatusSummary from '../components/StatusSummary/StatusSummary';
import moment from 'moment';
import { rapidAPIService, RapidAPIService } from '../services';
import { InfectedByProvince } from '../components/InfectedByProvince/InfectedByProvince';
import 'tui-chart/dist/maps/thailand';
import { InfectionMap } from '../components/InfectionMap/InfectionMap';
import { MapData } from '../types/provinces';
import { initGA, logPageView } from '../utils';

let chart: any = {};

export interface HomeState {
  infectedData: RapidAPIResponse;
  loadingInfectedData: boolean;
}

class Home extends Component<{}, HomeState> {

  DATE_FORMAT = 'DD MMMM YYYY (เวลา LT น.)';

  constructor(props: {}) {
    super(props);
    this.state = {
      infectedData: RapidAPIService.getInitalData(),
      loadingInfectedData: true,
    };
  }

  componentDidMount = (): void => {
    if (process.env.NODE_ENV === 'production') {
      if (!window.GA_INITIALIZED) {
        initGA();
        window.GA_INITIALIZED = true;
      }
      logPageView();
    }
    rapidAPIService.getTotal().then((infectedData: RapidAPIResponse) => {
      this.setState({
        infectedData,
        loadingInfectedData: false,
      });
    });
    chart = require('tui-chart');
  }

  loadMap = (data: MapData[]): void => {
    try {
      if (document) {
        const mapContainer = document.getElementById('map') as HTMLDivElement;
        const map = document.getElementById('mapInfection') as HTMLDivElement;
        const isMobile = window.innerWidth < 992;
        chart.mapChart(mapContainer, {
          series: data,
        }, {
          chart: { width: isMobile ? (window.innerWidth * 0.7) : 500, height: 514, format: '1,000' },
          map: 'thailand',
          legend: {
            align: isMobile ? 'bottom' : 'right'
          },
        });
        if (isMobile) {
          map.classList.add('pt-5');
        }
      }
    } catch (err) {
      console.warn(err);
    }
  }

  render = (): React.ReactElement => {
    const { infectedData, loadingInfectedData } = this.state;
    const date = moment(infectedData.response[0].time).locale('th');
    return (
      <div>
        <Head>
          <title>COVID-19 Reporter</title>
          <link rel='icon' href='/favicon.ico' />
        </Head>
        <NavBar />
        <div id='section-main' className='pt-5 pb-5'>
          <div className='container text-light'>
            <div className='row'>
              <div className='col-12'>
                <h1><span className='text-danger'>COVID-19</span> REPORTER</h1>
                <p>รายงานผู้ติดเชื้อ COVID-19 ในประเทศไทย</p>
              </div>
            </div>
            <div className='row'>
              <StatusSummary
                infectedData={infectedData}
                isLoading={loadingInfectedData}
              />
            </div>
            <div className='row'>
              <div className='col-12'>
                <div className='text-center'>ปรับปรุงล่าสุดเมื่อ {date.isValid() ? date.format(this.DATE_FORMAT) : ''}</div>
              </div>
            </div>
          </div>
        </div>
        <div id='section-data' className='pb-5'>
          <div className='container'>
            <div className='row'>
              <div className='table-wrap col-lg-4 col-12'>
                <InfectedByProvince />
              </div>
              <div id='mapInfection' className='col-lg-8 col-12'>
                <InfectionMap
                  loadMap={this.loadMap}
                />
              </div>
            </div>
          </div>
        </div>
        <div id='section-ref' className='pt-5'>
          <div className='container'>
            <div className='row text-light'>
              <div className='col-12'>
                <h4>แหล่งข้อมูล</h4>
                <p>กรมควบคุมโรค: <a className='text-light' href='https://covid19.th-stat.com/th' target='_blank'>https://covid19.th-stat.com/th</a></p>
              </div>
            </div>
          </div>
        </div>
        <footer className='footer mt-auto py-3 text-light'>
          <div className='container text-center'>
            <span>Copyright @ 2019 By Phoebus</span>
          </div>
        </footer>
      </div>
    );
  }
}

export default Home;
