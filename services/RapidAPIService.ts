import { RapidAPIResponse } from '../types';
import axios from 'axios';

export class RapidAPIService {
  public static getInitalData(): RapidAPIResponse {
    return {
      error: [],
      get: 'statistics',
      parameters: {
        country: ''
      },
      response: [{
        cases: {
          active: 0,
          critical: 0,
          new: 0,
          recovered: 0,
          newRecovered: 0,
          total: 0
        },
        deaths: {
          new: 0,
          total: 0,
        },
        day: '',
        time: '',
      }],
      results: 0,
    };
  }

  getTotal(): Promise<RapidAPIResponse> {
    return axios.get('/api/getTotal').then(res => res.data);
  }
}