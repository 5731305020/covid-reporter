import { CaseSumReport } from '../types';
import axios from 'axios';

export class DCAPIService {
    private caseSumReport = {} as CaseSumReport;

    getCaseSumData(): CaseSumReport {
        return this.caseSumReport;
    }

    getDataByProvince(): Promise<CaseSumReport> {
        if (Object.keys(this.caseSumReport).length > 0) {
            return Promise.resolve(this.caseSumReport);
        }
        return axios.get('/api/getCaseSum').then(res => {
            this.caseSumReport = res.data;
            return res.data;
        });
    }
}
