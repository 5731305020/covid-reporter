import { RapidAPIService } from './RapidAPIService';
import { DCAPIService } from './DCAPIService';

export const rapidAPIService = new RapidAPIService();
export const dcAPIService = new DCAPIService();

export * from './RapidAPIService';
export * from './DCAPIService';
