import React, { useEffect } from 'react';
import { dcAPIService } from '../../services';
import { getProvinceCode } from '../../utils';
import { MapData } from '../../types/provinces';

export interface InfectionMapProps {
  loadMap(data: MapData[]): void;
}

export const InfectionMap: React.FC<InfectionMapProps> = (props): React.ReactElement => {
  const { loadMap } = props;
  useEffect(() => {
    dcAPIService.getDataByProvince().then((caseSumData) => {
      const series: MapData[] = Object.keys(caseSumData.Province).map((key) => (
        {
          code: getProvinceCode(key),
          data: caseSumData.Province[key],
        }));
      loadMap(series);
    });
  }, []);
  return (
    <>
      <h4 className='text-light '>แผนที่แสดงจำนวนผู้ติดเชื้อในประเทศไทย</h4>
      <div id='map' className='bg-white' />
    </>
  );
};
