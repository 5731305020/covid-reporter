import React from 'react';

export const NavBar: React.FunctionComponent<{}> = (): React.ReactElement => {
  return (
    <nav className='navbar navbar-dark bg-dark'>
      <a className='navbar-brand' href='/'>
        <img src='/images/covid-virus.png' width='30' height='30' className='d-inline-block align-top mr-2' />
        COVID-19 REPORTER
      </a>
    </nav>
  );
};
