import React from 'react';
import { formatNumber } from '../../utils';

export interface StatusBlockProps {
  color: 'red' | 'danger' | 'yellow' | 'success' | 'secondary';
  title: string;
  amount: number;
  message?: string;
  isLoading: boolean;
}

export const StatusBlock: React.FC<StatusBlockProps> = (props): React.ReactElement => {
  const { color, title, amount, message, isLoading } = props;
  return (
    <div className={`card h-100 text-light text-center bg-${color}`}>
      <div className='card-body'>
        <h5 className='card-title'>{title}</h5>
        {!isLoading && <p className='display-4'>{formatNumber(amount)}</p>}
        {isLoading && <p className='display-4'><i className='fas fa-circle-notch fa-spin'/></p>}
        <small>{message}</small>
      </div>
    </div>
  );
};
