import React, { useState, useEffect, useCallback } from 'react';
import { CaseSumReport } from '../../types';
import { dcAPIService } from '../../services';
import { translateProvinceName, formatNumber } from '../../utils';

export const InfectedByProvince: React.FC<{}> = (): React.ReactElement => {
  const [caseData, setCaseData] = useState<CaseSumReport>({} as CaseSumReport);
  useEffect((): void => {
    dcAPIService.getDataByProvince().then((data) => {
      setCaseData(data);
    });
  }, []);
  const amounts: number[] = caseData.Province ? Object.keys(caseData.Province).map(key => caseData.Province[key]) : [0];
  const totalAmount = amounts.reduce((total, curr) => total + curr, 0);
  let resultData: React.ReactElement;
  const renderRowData = useCallback((item: string): React.ReactElement => {
    return (
      <tr key={item}>
        <td>{item === 'Unknown' ? 'ยังระบุไม่ได้' : translateProvinceName(item)}</td>
        <td className='text-right'>{formatNumber(caseData.Province[item])}</td>
      </tr>
    );
  }, [caseData]);
  if (caseData.Province) {
    resultData = <>{Object.keys(caseData.Province).map((item) => (renderRowData(item)))}</>;
  } else {
    resultData = (
      <tr>
        <td colSpan={2} className='text-center'><i className='fas fa-circle-notch fa-spin' /> กำลังโหลดข้อมูล</td>
      </tr>
    );
  }
  return (
    <>
      <h4 className='text-light position-sticky bg-dark mb-0 pb-2 fixed-top'>ผู้ติดเชื้อแยกตามจังหวัด</h4>
      <table id='province-list' className='table table-light table-striped table-hover table-fixed'>
        <thead className='thead-light'>
          <tr>
            <th>จังหวัด</th>
            <th>จำนวนผู้ติดเชื้อ</th>
          </tr>
        </thead>
        <tbody>
          {resultData}
          <tr>
            <td>รวมทั้งหมด</td>
            <td className='text-right'>{formatNumber(totalAmount)}</td>
          </tr>
        </tbody>
      </table>
    </>
  );
};