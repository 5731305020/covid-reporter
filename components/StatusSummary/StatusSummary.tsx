import React from 'react';
import { StatusBlock } from '../StatusBlock/StatusBlock';
import { RapidAPIResponse } from '../../types';
import { formatInfectedData } from '../../utils';

export interface StatusSummaryProps {
  infectedData: RapidAPIResponse;
  isLoading: boolean;
}

export const StatusSummary: React.FC<StatusSummaryProps> = (props): React.ReactElement => {
  const {infectedData, isLoading } = props;
  const formatedData = formatInfectedData(infectedData, isLoading);
  const classForMobile = 'col-md-6 col-lg-3 col-4 mb-3';
  const classForAll = 'col-md-6 col-lg-3 col-12 mb-3';
  return (
    <>
      {formatedData.map((data, index) => (<div key={data.title} className={`status-block ${index === 0 ? classForAll : classForMobile}`}><StatusBlock {...data} /></div>))}
    </>
  );
};

export default StatusSummary;