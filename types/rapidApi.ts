export interface PatientCase {
    new: number;
    active: number;
    critical: number;
    recovered: number;
    newRecovered: number;
    total: number;
}

export interface DeathCase {
    new: number;
    total: number;
}

export interface Case {
    cases: PatientCase;
    deaths: DeathCase;
    day: string;
    time: string;
}

export interface RapidAPIResponse {
    get: 'statistics' | 'countries' | 'history';
    parameters: {
        country: string;
    };
    error: [];
    results: number;
    response: Case[];
}

export interface FormatedStat extends PatientCase {
    treated: number;
}