export interface DailyReport {
    Confirmed: number;
    Recovered: number;
    Hospitalized: number;
    Deaths: number;
    NewConfirmed: number;
    NewRecovered: number;
    NewHospitalized: number;
    NewDeaths: number;
    UpdateDate: string;
    Source: string;
}

export interface CaseSumReport {
    Province: {
        [key: string]: number;
    };
    Nation: {
        [key: string]: number;
    };
    Gender: {
        Male: number;
        Female: number;
        Unknown: number;
    };
    LastData: string;
    Source: string;
}