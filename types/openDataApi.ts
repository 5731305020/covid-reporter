export interface Field {
    type: string;
    id: string;
}

export interface Record {
    _id: string;
    Province: string;
    'Count of no': string;
}

export interface Result {
    include_total: boolean;
    resource_id: string;
    fields: Field[];
    records_format: string;
    records: Record[];
    total: number;
}

export interface OpenDataAPIResponse {
    success: boolean;
    result: Result;
}