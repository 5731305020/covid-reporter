/// <reference types="next" />
/// <reference types="next/types/global" />
declare namespace NodeJS {
    export interface ProcessEnv {
        API_KEY: string;
        RESOURCE_DAILY_ID: string;
        RESOURCE_ACCUMULATE_ID: string;
        APP_ID: string;
        APP_API_KEY: string;
    }
}