export const config = {
    rapidApiEndpoint: 'https://covid-193.p.rapidapi.com/statistics',
    rapidApiHost: 'covid-193.p.rapidapi.com',
    rapiadApiKey: process.env.APP_API_KEY,
    dataApiEndpoint: 'https://opend.data.go.th/get-ckan/datastore_search',
    dataApiKey: process.env.API_KEY,
    resourceDailyId: process.env.RESOURCE_DAILY_ID,
    resourceAccumulateId: process.env.RESOURCE_ACCUMULATE_ID,
    dcDailyReportEndpoint: 'https://covid19.th-stat.com/api/open/today',
    dcCaseSumReportEndpoint: 'https://covid19.th-stat.com/api/open/cases/sum',
};
