import { RapidAPIResponse } from '../types';
import { StatusBlockProps } from '../components/StatusBlock/StatusBlock';
import { thaiProvinces, thaiProvinceCode } from '../types/provinces';

export const formatInfectedData = (infectedData: RapidAPIResponse, isLoading: boolean): StatusBlockProps[] => {
  const data = infectedData.response[0];
  const { cases: { total, new: newCase, active, recovered, newRecovered, critical }, deaths: { total: deathTotal, new: newDeaths } } = data;
  return [{
    color: 'danger',
    title: 'ผู้ติดเชื้อ',
    amount: total,
    message: newCase ? `เพิ่มขึ้น ${formatNumber(newCase)} คน` : '',
    isLoading,
  }, {
    color: 'yellow',
    title: 'กำลังรักษา',
    amount: active,
    message: `อาการสาหัส ${formatNumber(critical)} คน`,
    isLoading,
  }, {
    color: 'success',
    title: 'หายแล้ว',
    amount: recovered,
    message: `เพิ่มขึ้น ${formatNumber(newRecovered)} คน`,
    isLoading,
  }, {
    color: 'secondary',
    title: 'เสียชีวิต',
    amount: deathTotal,
    message: newDeaths ? `เพิ่มขึ้น ${formatNumber(newDeaths)} คน` : '',
    isLoading,
  }];
};

export const formatNumber = (amount: number): string => {
  return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const translateProvinceName = (provinceName: string): string => {
  return thaiProvinces[provinceName] ? thaiProvinces[provinceName] : provinceName;
};

export const getProvinceCode = (provinceName: string): string => {
  return thaiProvinceCode[provinceName] || '';
};
